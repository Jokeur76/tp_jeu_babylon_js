var lapin, lapinBox, animLapin, animMarcheLapin, animMortLapin, tache, codeToucheSaut = 32, vitesse = 5, obsArrays = [], bonusArrays = [], minObsPosX = 0, maxObsPosX = 500, minObsPosY = 0, maxObsPosY = 100, minObsHeight = 20, maxObsHeight = 50, minDist = 500, maxDist = 1000, lastObsPosition = -1, lgSol = 20000000000000000000000000000000000, htLapin = 60, sautMaxLapin = 100, touchParticleSystem, tpsTimer, gameEnded = false;
document.addEventListener("DOMContentLoaded", function () {
    var canvas = document.getElementById("renderCanvas");
    var engine = new BABYLON.Engine(canvas, true);
    var scene = new BABYLON.Scene(engine);
    scene.collisionsEnabled = true;
    var camera = new BABYLON.FreeCamera("freeCamera", new BABYLON.Vector3(300, 0, -1000), scene);
    //camera.rotation.y = BABYLON.Tools.ToRadians(180);
    //var light = new BABYLON.PointLight ("mainLight", new BABYLON.Vector3(0, 0, 0), scene);
    var light = new BABYLON.HemisphericLight("mainLight", new BABYLON.Vector3(1, 0, 0), scene);
    camera.attachControl(canvas);
    var loader = new BABYLON.AssetsManager(scene);
    var meshTask = loader.addMeshTask("meshTask", "", "/lapin/", "Rabbit.babylon");
    // Fonction appelée quand le chargement de l’objet est terminé
    meshTask.onSuccess = function (task) {
        tache = task;
        lapin = createLapin(scene);
        addListeners(scene);
        camera.setTarget(lapin);
    };
    loader.onFinish = function (tasks) {
        createFloor(scene, lapin);
        createObstacles(scene, lapin);
        createbonus(scene, lapin);
        document.getElementById("lives").innerHTML = lapin.lives.toString() + " vies";
        engine.runRenderLoop(function () {
            scene.render();
            if (lapin.alive) {
                vitesse = calcSpeed(lapin.position.x);
                calcScore(lapin.position.x);
                document.getElementById("speedValue").innerHTML = vitesse.toString();
                lapin.position.x += vitesse;
                camera.position.x += vitesse;
                manageObstacles(scene, lapin);
            }
            else {
                if (lapin.position.y > 0) {
                    lapin.position.y -= 1;
                }
                else {
                    if (!gameEnded) {
                        var skeletons = tache.loadedSkeletons;
                        skeletons.forEach(function (s) {
                            scene.beginAnimation(s, 70, 100);
                        });
                        document.getElementById("youdie").setAttribute("style", "display: block");
                        document.getElementById("finalScore").innerHTML = (lapin.position.x / 10).toString();
                        gameEnded = true;
                    }
                }
            }
        });
    };
    loader.load(); // Démarre le chargement
}, false);
function calcSpeed(positionLapin) {
    if (positionLapin < 10000) {
        return Math.floor(5 + positionLapin / 2000);
    }
    else if (positionLapin < 25000) {
        minDist = 750;
        maxDist = 1500;
        return Math.floor(10 + ((positionLapin - 10000) / 3000) * 2);
    }
    else if (positionLapin < 50000) {
        minDist = 1000;
        maxDist = 2000;
        return Math.floor(20 + ((positionLapin - 25000) / 5000) * 3);
    }
    else {
        minDist = 1500;
        maxDist = 5000;
        return 40;
    }
}
function calcScore(positionLapin) {
    document.getElementById("scoreValue").innerHTML = (Math.floor(positionLapin / 10)).toString();
}
function createLapin(scene) {
    var lapin = tache.loadedMeshes[0];
    lapin.rotation.y = BABYLON.Tools.ToRadians(90);
    lapin.checkCollisions = true;
    lapin['lives'] = 3;
    lapin['alive'] = true;
    var animSaut = new BABYLON.Animation("animSaut", "position.y", 100, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_RELATIVE);
    var easingFunction = new BABYLON.SineEase();
    var keys = [];
    keys.push({
        frame: 1,
        value: 0
    }, {
        frame: 50,
        value: sautMaxLapin
    }, {
        frame: 100,
        value: 0
    });
    animSaut.setKeys(keys);
    easingFunction.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
    animSaut.setEasingFunction(easingFunction);
    lapin.animations.push(animSaut);
    lapinBox = BABYLON.Mesh.CreateBox("lapinbox", htLapin, scene);
    lapinBox.scaling.z = 0.50;
    lapinBox.scaling.x = 0.50;
    lapinBox.position.y += htLapin / 2;
    lapinBox.position.z += htLapin / 4;
    lapinBox.parent = lapin;
    lapinBox.isVisible = false;
    var skeletons = tache.loadedSkeletons;
    skeletons.forEach(function (s) {
        scene.beginAnimation(s, 0, 50, true);
    });
    touchParticleSystem = new BABYLON.ParticleSystem("touchParticleSystem", 1000, scene);
    touchParticleSystem.particleTexture = new BABYLON.Texture("images/goutte_sang.png", scene);
    touchParticleSystem.gravity = new BABYLON.Vector3(0, -9.8, 0);
    touchParticleSystem.emitRate = 200;
    touchParticleSystem.minSize = 10;
    touchParticleSystem.maxSize = 15;
    touchParticleSystem.direction1 = new BABYLON.Vector3(60, -60, 0);
    touchParticleSystem.direction2 = new BABYLON.Vector3(60, 60, 0);
    touchParticleSystem.emitter = lapinBox;
    return lapin;
}
function createFloor(scene, lapin) {
    var solHaut = BABYLON.Mesh.CreateCylinder("solHaut", lgSol, 16, 16, 4, 4, scene);
    solHaut.rotation.z = BABYLON.Tools.ToRadians(90);
    solHaut.rotation.x = BABYLON.Tools.ToRadians(45);
    solHaut.position.x = lapin.position.x + 500;
    solHaut.position.y = -8;
}
function addListeners(scene) {
    window.addEventListener('keydown', function (event) {
        if (event.keyCode == codeToucheSaut && lapin.position.y == 0) {
            scene.beginAnimation(lapin, 1, 200);
        }
    });
}
function manageObstacles(scene, lapin) {
    //make life bonus
    var coeur = bonusArrays[0];
    if (Math.random() > 0.999 && coeur.position.x < lapin.position.x - 400) {
        coeur.position.x = lapin.position.x + 1000;
        coeur.isVisible = true;
    }
    var movePossible = false;
    for (var obs of obsArrays) {
        if (obs.position.x < lapin.position.x - 400) {
            movePossible = true;
            break;
        }
    }
    if (movePossible) {
        var obsPosX = lapin.position.x + 1000 + Math.floor(Math.random() * (maxObsPosX - minObsPosX)) + minObsPosX;
        if (lastObsPosition < 0 || (obsPosX - lastObsPosition > minDist && obsPosX - lastObsPosition < maxDist)) {
            var obsHeight = Math.floor(Math.random() * (maxObsHeight - minObsHeight)) + minObsHeight;
            var obstacle;
            var index = 0;
            do {
                index = Math.floor(Math.random() * (obsArrays.length - 1));
                obstacle = obsArrays[index];
            } while (obstacle.position.x > lapin.position.x - 400);
            obstacle.position.x = obsPosX;
            //set y
            if (Math.random() > 0.5) {
                //obj à sauter par dessus
                var htObsCorrected = Math.floor(Math.random() * (sautMaxLapin - obstacle.scaling.y)) + obstacle.scaling.y;
                obstacle.scaling.y = htObsCorrected - 10;
                obstacle.position.y = obstacle.scaling.y / 2;
            }
            else {
                //obj a passer en dessous
                var htMin = htLapin + obstacle.scaling.y / 2 + 10;
                var htMax = htMin + 50;
                obstacle.position.y = Math.floor(Math.random() * (htMax - htMin)) + htMin;
            }
            lastObsPosition = obsPosX;
            obstacle.actionManager = new BABYLON.ActionManager(scene);
            var trigger = { trigger: BABYLON.ActionManager.OnIntersectionEnterTrigger, parameter: lapinBox };
            var eca = new BABYLON.ExecuteCodeAction(trigger, function (actionEvent) {
                if (tpsTimer)
                    clearTimeout(tpsTimer);
                touchParticleSystem.start();
                tpsTimer = setTimeout(function () {
                    touchParticleSystem.stop();
                }, 500);
                lapin.lives -= 1;
                lapin.alive = lapin.lives > 0;
                document.getElementById("lives").innerHTML = lapin.lives.toString() + " vies";
            });
            obstacle.actionManager.registerAction(eca);
        }
    }
    var boundingTest = new BABYLON.BoundingBox(new BABYLON.Vector3(0, 0, 0), new BABYLON.Vector3(1, 1, 1));
    if (obstacle && coeur && coeur.intersectsMesh(obstacle)) {
        coeur.position.x += 30;
    }
}
function createObstacles(scene, lapin) {
    var obsHeigths = [];
    for (var i = 1; i <= 10; i++) {
        obsHeigths.push(minObsHeight + i * 3);
    }
    for (var height of obsHeigths) {
        var obstacle = BABYLON.Mesh.CreateBox("obstacle", 1, scene);
        obstacle.scaling.y = height;
        obstacle.scaling.x = height / 2;
        obstacle.scaling.z = 15;
        obstacle.position.x = lapin.position.x - 500;
        obsArrays.push(obstacle);
    }
}
function createbonus(scene, lapin) {
    var coeurHt = 30;
    var bonus = BABYLON.Mesh.CreateBox("obstacle", coeurHt, scene);
    bonus.position.x = lapin.position.x - 500;
    var materialCoeur = new BABYLON.StandardMaterial("materialCoeur", scene);
    materialCoeur.diffuseTexture = new BABYLON.Texture("images/coeur.png", scene);
    bonus.material = materialCoeur;
    bonus.position.x = lapin.position.x - 500;
    bonus.position.y = Math.floor(Math.random() * ((sautMaxLapin + htLapin - 10) - coeurHt / 2)) + coeurHt / 2;
    bonusArrays.push(bonus);
    bonus.actionManager = new BABYLON.ActionManager(scene);
    var trigger = { trigger: BABYLON.ActionManager.OnIntersectionEnterTrigger, parameter: lapinBox };
    var eca = new BABYLON.ExecuteCodeAction(trigger, function (actionEvent) {
        lapin.lives += 1;
        bonus.isVisible = false;
        document.getElementById("lives").innerHTML = lapin.lives.toString() + " vies";
    });
    bonus.actionManager.registerAction(eca);
}
//# sourceMappingURL=index.js.map