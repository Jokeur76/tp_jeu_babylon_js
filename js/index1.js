document.addEventListener("DOMContentLoaded", function () {
    var canvas = document.getElementById("renderCanvas");
    var engine = new BABYLON.Engine(canvas, true);
    var scene = new BABYLON.Scene(engine);
    var camera = new BABYLON.FreeCamera("freeCamera", new BABYLON.Vector3(0, 0, -1000), scene);
    //camera.rotation.y = BABYLON.Tools.ToRadians(180);
    //var light = new BABYLON.PointLight ("mainLight", new BABYLON.Vector3(0, 0, 0), scene);
    var light = new BABYLON.HemisphericLight("mainLight", new BABYLON.Vector3(0, 1, 1), scene);
    camera.attachControl(canvas);
    engine.runRenderLoop(function () {
        scene.render();
    });
    var sphereRouge = BABYLON.Mesh.CreateSphere("sphereRouge", 100, 200, scene);
    var material = new BABYLON.StandardMaterial("rouge", scene);
    material.diffuseColor = BABYLON.Color3.Red();
    sphereRouge.material = material;
    sphereRouge.actionManager = new BABYLON.ActionManager(scene);
    var ifRed = new BABYLON.PredicateCondition(sphereRouge.actionManager, function () {
        return sphereRouge.material.diffuseColor.equals(BABYLON.Color3.Red());
    });
    var ifGreen = new BABYLON.PredicateCondition(sphereRouge.actionManager, function () {
        return sphereRouge.material.diffuseColor.equals(BABYLON.Color3.Green());
    });
    var ifBlue = new BABYLON.PredicateCondition(sphereRouge.actionManager, function () {
        return sphereRouge.material.diffuseColor.equals(BABYLON.Color3.Blue());
    });
    sphereRouge.actionManager.registerAction(new BABYLON.InterpolateValueAction(BABYLON.ActionManager.OnPickTrigger, sphereRouge.material, "diffuseColor", BABYLON.Color3.Green(), 1000, ifRed));
    sphereRouge.actionManager.registerAction(new BABYLON.InterpolateValueAction(BABYLON.ActionManager.OnPickTrigger, sphereRouge.material, "diffuseColor", BABYLON.Color3.Blue(), 1000, ifGreen));
    sphereRouge.actionManager.registerAction(new BABYLON.InterpolateValueAction(BABYLON.ActionManager.OnPickTrigger, sphereRouge.material, "diffuseColor", BABYLON.Color3.Red(), 1000, ifBlue));
}, false);
//# sourceMappingURL=index1.js.map